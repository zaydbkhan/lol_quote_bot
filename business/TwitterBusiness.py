from data.TweetData import TweetData
import nltk
from nltk.corpus import stopwords
from nltk.corpus import wordnet
from nltk.stem import WordNetLemmatizer
import re

class TwitterBusiness:

    def __init__(self, twt_api):
        self.api = twt_api
    
    def qt_retweet(self, quote, twt_id, champion):

        tweetdata = TweetData()

        self.api.PostUpdate(f'"{quote}"', attachment_url=f'https://twitter.com/anyuser/status/{twt_id}')
        self.api.CreateFavorite(status_id=twt_id)
        print('Tweet successful')

        tweetdata.add_used_tweet(twt_id, quote, champion)

    def search(self, search_term, min_faves, since, num_tweets):
        return self.api.GetSearch(f'{search_term} min_faves:{min_faves} since:{since}', lang='en', count=num_tweets, return_json=True)

    def get_possible_tweets(self, tweets, search_term):
        tweetdata = TweetData()

        search_results = tweets['statuses']
        good_tweets = []

        for v in search_results:
            l = {}
            l['id'] = v['id']
            l['favorites'] = v['favorite_count']
            l['handle'] = v['user']['screen_name']
            l['text'] = self.get_keywords(v['full_text'])
            l['full_text'] = v['full_text']
            if self.check_banned_phrases(l['text']):
                tweetdata.add_tweet(l['id'], search_term, l['handle'], l['favorites'], l['text'], l['full_text'], False)
            else:
                good_tweets.append(l)
                tweetdata.add_tweet(l['id'], search_term, l['handle'], l['favorites'], l['text'], l['full_text'], True)

        return good_tweets

    def pick_best_tweet(self, tweets):
        most_favorited = {'favorites': 0}
        for x in tweets:
            if(x['favorites'] > most_favorited['favorites']):
                most_favorited = x
        return most_favorited

    def __pos_tagger(self, tag):
        if tag.startswith('J'):
            return wordnet.ADJ
        elif tag.startswith('V'):
            return wordnet.VERB
        elif tag.startswith('N'):
            return wordnet.NOUN
        elif tag.startswith('R'):
            return wordnet.ADV
        else:          
            return None

    def get_keywords(self, text):
        stop_words = set(stopwords.words('english'))
        lem = WordNetLemmatizer()
        text = re.sub(r"\W", " ", text)
        text = re.sub(r"\s+", " ", text)
        text = text.lower()
        text = nltk.pos_tag(nltk.word_tokenize(text))
        text = list(map(lambda x: (x[0], self.__pos_tagger(x[1])), text))

        l_text = []
        for w, t in text:
            if t is None:
                l_text.append(w)
            else:
                l_text.append(lem.lemmatize(w, t))

        p_text = []
        for w in l_text:
            if w not in stop_words:
                p_text.append(w)
        
        p_text = " ".join(p_text)
        return p_text

    def check_banned_phrases(self, text):
        #Returns True on found banned phrase
        banned_phrases = open('banned_phrases.txt', 'r')
        for l in banned_phrases:
            if str.rstrip(l) in text:
                banned_phrases.close()
                return True
            else:
                pass
        banned_phrases.close()
        return False