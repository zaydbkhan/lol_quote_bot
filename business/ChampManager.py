from data.QuoteData import QuoteData
import random

class ChampManager:

    def __init__(self):
        self.quote_data = QuoteData()

    def get_random_champ(self):
        return random.choice(self.quote_data.get_act_champs())

    def get_random_quote(self, champ_name):
        return random.choice(self.quote_data.get_quotes(champ_name))

    def bulk_upsert(self, champ_name, quotes):
        for q in quotes:
            self.quote_data.add_quote(champ_name, q)