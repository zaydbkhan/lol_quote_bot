from data.WikiData import WikiData

class ChampProvider:

    def __init__(self):
        self.wiki_data = WikiData()

    def get_champs(self):
        return self.wiki_data.get_champ_list()

    def get_quotes(self, champ):
        return self.wiki_data.get_champ_quotes(champ)