import twitter as twt
import db
from service.TwitterService import TwitterService
import os

api = twt.Api(consumer_key=os.getenv('CONSUMER_KEY'),
    consumer_secret=os.getenv('CONSUMER_SECRET'),
    access_token_key=os.getenv('ACCESS_TOKEN'),
    access_token_secret=os.getenv("ACCESS_SECRET"),
    tweet_mode = 'extended')

db.open_db()

twitterbusiness = TwitterService(api)

twitterbusiness.qt_retweet()

db.close_db()

# TODO: 
# none for now

# print(api.GetSearch('Aatrox min_faves:250 since:2022-02-10',  lang='en', count=50, return_json=True))