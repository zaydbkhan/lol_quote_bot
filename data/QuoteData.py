import random
from datetime import datetime as dt
from db import *

class QuoteData:

    def get_quotes(self, champion):
        quotes = []
        for quote in Quote.select().where(Quote.champion_name == champion):
            quotes.append(quote.champion_quote)
        return quotes

    def get_act_champs(self):
        act_champs = []
        for champion in Quote.select(Quote.champion_name).distinct():
            act_champs.append(champion.champion_name)
        return act_champs

    def add_quote(self, champ_name, quote):
        now = dt.now()
        Quote.get_or_create(
            champion_name = champ_name,
            champion_quote = quote,
            defaults = {'create_dt': now, 'update_dt': now}
        )