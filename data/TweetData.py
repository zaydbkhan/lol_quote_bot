from db import *
from datetime import datetime as dt

class TweetData:

    def add_tweet(self, id, search_term, handle, favorites, text, full_text, eligible):
        now = dt.now()
        Tweet.get_or_create(
                tweet_id = id,
                defaults = {
                    'searched': search_term,
                    'handle': handle,
                    'favorites': favorites,
                    'text': text,
                    'full_text': full_text,
                    'eligible': eligible,
                    'create_dt': now, 
                    'update_dt': now}
            )

    def add_used_tweet(self, id, quote, champion):
        now = dt.now()
        UsedTweet.get_or_create(
            tweet_id = id,
            defaults = {
                'tweeted': quote,
                'tweeted_champion': champion,
                'create_dt': now,
                'update_dt': now
            }
        )