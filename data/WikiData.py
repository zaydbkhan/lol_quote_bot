from db import *
import requests
import re

class WikiData:

    def get_champ_quotes(self, champ_name):
        quotes = []
        page = f"{champ_name}/LoL/Audio"
        api = f"https://leagueoflegends.fandom.com/api.php?action=query&prop=revisions&titles={page}&rvslots=*&rvprop=content&formatversion=2&format=json"
        response = requests.get(api)

        response_json = response.json()
        content_str = response_json["query"]["pages"][0]["revisions"][0]["slots"]["main"]["content"]
        content_str_no_trivia = content_str.partition("==Trivia==")[0] # removes the trivia section to stop regex from picking up non quotes
        content_lns = content_str_no_trivia.splitlines()
        content_lns_cut = []

        for ln in content_lns:
            if "|-|" in ln:
                break
            else:
                content_lns_cut.append(ln)

        for ln in content_lns_cut:
            m = re.search(r"\'+\"([\w\s\.\,\?\!\'\-\;\:]+)\"", ln)
            if m and len(m.groups()) == 1:
                quotes.append(m.group(1))

        final = []

        for q in list(quotes):
            if "ogg" in q:
                pass
            elif("'''" in q):
                final.append(q.replace("'''", ""))
            else:
                final.append(q)
    
        return final

    def get_champ_list(self):
        champions = []
        api = f"https://leagueoflegends.fandom.com/api.php?action=query&prop=revisions&titles=List_of_champions&rvslots=*&rvprop=content&formatversion=2&format=json"
        response = requests.get(api)

        response_json = response.json()
        content_str = response_json["query"]["pages"][0]["revisions"][0]["slots"]["main"]["content"]
        content_lns = content_str.splitlines()

        for ln in content_lns:
            m = re.search(r"row\|([\w\s\.\,\?\!\'\&]+)(\||\}\})", ln)
            if m and len(m.groups()) == 2:
                champions.append(m.group(1))
    
        for index, name in enumerate(champions):
            champions[index] = name.replace(" ", "_")

        return champions