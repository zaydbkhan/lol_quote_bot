peewee==3.15.0
pymysql==1.0.2
python-dotenv==0.20.0
python_twitter==3.5
nltk==3.7
cryptography==37.0.2