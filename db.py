from peewee import *
import os
from dotenv import load_dotenv

load_dotenv()

def open_db():
    _db.connect(reuse_if_open=True)

def close_db():
    _db.close()

def init_db():
    _db.create_tables([Quote, Tweet, UsedTweet])
    q_idx = Quote.index(Quote.champion_quote, Quote.champion_name)
    t_idx = Tweet.index(Tweet.text, Tweet.eligible, Tweet.searched)
    u_idx = UsedTweet.index(UsedTweet.tweet_id, UsedTweet.tweeted_champion)

_db = MySQLDatabase(
    host='127.0.0.1',
    user='root',
    password=os.getenv('DB_PASSWORD'),
    database='lol_quotes'
)

class Quote(Model):
    champion_name = CharField()
    champion_quote = TextField()
    create_dt = DateField()
    update_dt = DateField()

    class Meta:
        database = _db

class Tweet(Model):
    tweet_id = BigIntegerField(primary_key=True)
    searched = CharField()
    handle = CharField()
    favorites = IntegerField()
    text = TextField()
    full_text = TextField()
    eligible = BooleanField()
    create_dt = DateField()
    update_dt = DateField()

    class Meta:
        database = _db

class UsedTweet(Model):
    tweet_id = BigIntegerField(primary_key=True)
    tweeted = TextField()
    tweeted_champion = CharField()
    create_dt = DateField()
    update_dt = DateField()

    class Meta:
        database = _db