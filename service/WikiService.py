from business.ChampManager import ChampManager
from business.ChampProvider import ChampProvider

class WikiService:

    def __init__(self):
        self.champ_mgr = ChampManager()
        self.champ_prov = ChampProvider()

    def update_db(self):
        champs = self.champ_prov.get_champs()

        for c in champs:
            self.champ_mgr.bulk_upsert(c, self.champ_prov.get_quotes(c))