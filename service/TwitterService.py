from business.TwitterBusiness import TwitterBusiness
from business.ChampManager import ChampManager
from datetime import datetime as dt, timedelta

class TwitterService:

    def __init__(self):
        self.champ_mgr = ChampManager()
        self.twt_service = TwitterBusiness()

    def qt_retweet(self):

        # move to constants
        numlikes = 1000

        champ = self.champ_mgr.get_random_champ()
        quote = self.champ_mgr.get_random_quote()

        t = dt.now() - timedelta(days=7)

        x = self.twt_service.search(champ, numlikes, t.strftime('%Y-%m-%d'), 50)

        # temp algorithm to ensure selected champ is tweeted
        if (len(x['statuses']) == 0):
            while (len(x['statuses']) == 0):
                numlikes = numlikes / 2
                x = self.twt_service.search(champ, numlikes, t.strftime('%Y-%m-%d'), 50)

        search_results = self.twt_service.get_possible_tweets(x, champ)
        tweet = self.twt_service.pick_best_tweet(search_results)

        s_twt_id = tweet['id']

        print(tweet)
        print(f'"{quote}"')
        print(f'https://twitter.com/anyuser/status/{s_twt_id}')

        do_tweet = input()

        if do_tweet == 'y':
            self.twt_service.qt_retweet(quote, s_twt_id, champ)

        else:
            print('Tweet aborted')